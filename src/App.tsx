import React from 'react';
import Heading from "./components/Heading/Heading"
import Questions from "./components/Questions/Questions"
function App() {
  return (
    <div className="App">
      <Heading/>
      <Questions/>
    </div>
  );
}

export default App;
