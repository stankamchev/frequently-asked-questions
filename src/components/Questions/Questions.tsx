import React,{useState} from 'react'
import "./Questions.css"
import {IoMdArrowDropdown, IoMdArrowDropup} from "react-icons/io"

const Question = () => {
    const [isActive, setIsActive] = useState<null>(null)
    
    let questionNum :number[] = [...Array(10).keys()]

    const toggleActive = (num: any) : void => {
        if(isActive === num){
            setIsActive(null)
        } else {
            setIsActive(num)  
        }
    }
    
    return (      
        <div className="question-wrapper">
            {questionNum.map((num : number)=>{
                return(
                <div key={num}>
                <div onClick={()=>toggleActive(num)} className="container-wrapper">
                    <h1 className="question-text">Question number {num}</h1>
                    {isActive === num ?    
                        <i className="icon"><IoMdArrowDropup/></i>
                    :
                        <i className="icon"><IoMdArrowDropdown/></i>}
                </div>
                
                <div 
                
                className={
                    isActive === num ? "active" : "hidden"
                }>
                    <p className="answer">
                        Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
                    </p>
                </div>
                </div>

                )
            })}
        </div>
    )
}

export default Question
