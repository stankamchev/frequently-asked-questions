import React from 'react'
import "./Heading.css"
const Heading = () => {
    return (
        <div className="text-wrapper">
            <h1 className="heading">Frequently asked questions</h1>
            <p className="paragraph">Hello! Didn't find what you are looking for? Please contact us.</p>   
        </div>
    )
}

export default Heading
